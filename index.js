// file: ./examples/pm
var program = require('commander');

//The commander will try to search the executables in the directory of the entry script (like ./examples/pm) with the name program-command, like pm-install, pm-search.

program
    .version('0.1.0')
    .command('download [name] [filename]', 'download subreddit from bigquery')
    .command('crawl [filename]', 'crawl filename using amazon crawled')
    .command('list', 'list packages installed', {isDefault: true})
    .parse(process.argv);
